<?php
				require_once "inc/class_db.php"; 
				require_once "inc/start_session.php";
		try 
		{
			$db = new DB;
			
			$stmt ='';
			
			$title='';
			$start='';
			$end='';
			$atLeast=NULL;
			$count = NULL;
			$worked=0;
			
			if(count($_GET) > 0)
			{
				$query = "SELECT M.title,M.year FROM Movies M ";
				$orig = strlen($query)-1;
				$query = "$query WHERE ";
				$ifEnd='';
				$starting=0;

					if($_GET["movie_title"]!=NULL)
					{
						$title = $_GET["movie_title"];
						$query = $query . "M.title LIKE '%$title%'";
						
						$ifEnd="AND";

					}
					
					if($_GET["movie_year_start"]!=NULL && $_GET["movie_year_end"]!=NULL)
					{
						$start = $_GET["movie_year_start"];
						$end = $_GET["movie_year_end"];
						$query = $query . "$ifEnd M.year BETWEEN '$start' AND '$end'";
						$ifEnd=" AND";
					}
					else if($_GET["movie_year_start"]==NULL && $_GET["movie_year_end"]!=NULL)
					{
						$end = $_GET["movie_year_end"];
						$query = $query . "$ifEnd M.year='$end'";
						$ifEnd=" AND";
					}
					else if($_GET["movie_year_start"]!=NULL && $_GET["movie_year_end"]==NULL)
					{
						$start = $_GET["movie_year_start"];
						$query = $query . "$ifEnd M.year='$start'";
						$ifEnd=" AND";
					}
					
					
					
					if(isset($_GET["mpaa_rating"]))
					{
						$array = $_GET["mpaa_rating"];
						$implodedArray = implode("','",$array);
						$implodedArray = "'$implodedArray'";
						$query = $query . "$ifEnd M.mpaa_rating IN ($implodedArray)";
						$ifEnd=" AND";
					}
					
					if(isset($_GET["genre"]))
					{
						$join = " LEFT JOIN Catalog_Genre G ON M.title=G.movie_title AND M.year=G.movie_year ";
						$length =  strlen($join);
						$query = substr($query,0,$orig+$starting) . $join . substr($query,$orig+$starting);
						$starting = $starting + $length;
						$array = $_GET["genre"];
						$implodedArray = implode("','",$array);
						$implodedArray = "'$implodedArray'";
						$query = $query . "$ifEnd G.genre IN ($implodedArray)";
						$ifEnd=" AND";
						$starting = $starting + strlen($join);
					}

					if($_GET['rating']!=NULL || $_GET['count']!=NULL)
					{
						$join = " LEFT JOIN RatingsCounts R ON R.movie_title=M.title AND R.movie_year=M.year ";
						$length =  strlen($join);
						$query = substr($query,0,$orig+$starting) . $join . substr($query,$orig+$starting);
						$starting = $starting + $length;
						if($_GET['rating']!=NULL)
						{
							$atLeast = $_GET['rating'];
							$query = $query . "$ifEnd R.average_rating >= $atLeast";
							$ifEnd=" AND";
						}
						if($_GET['count']!=NULL)
						{
							$count = $_GET['count'];
							$query = $query . "$ifEnd R.count >= $count";
							$ifEnd=" AND";
						}
					}
						
					$array = $_GET["Actorname"] + $_GET["Directorname"] + $_GET["Producersname"] ;
					$x=0;
					foreach($array as $test)
					{
						if($test == '')
							unset($array[$x]);
						$x++;
					}
					
					if(count($array)>=1)
					{
						$join = " LEFT JOIN Catalog_Cast C ON M.title=C.movie_title AND M.year=C.movie_year";
						$length =  strlen($join);
						$query = substr($query,0,$orig+$starting) . $join . substr($query,$orig+$starting);
						$starting = $starting + $length;
						$implodedArray = implode("','",$array);
						$implodedArray = "'$implodedArray'";
						$query = $query . "$ifEnd C.people_name IN ($implodedArray)";
						$ifEnd=" AND";
					}
					

					$array = $_GET["keys"];
					$x=0;
					foreach($array as $test)
					{
						if($test == '')
							unset($array[$x]);
						$x++;
					}
					
					if(count($array)>=1)
					{
						$join = " LEFT JOIN Catalog_Keywords K ON M.title=K.movie_title AND M.year=K.movie_year";
						$length =  strlen($join);
						$query = substr($query,0,110+$starting) . $join . substr($query,110+$starting);
						$starting = $starting + $length;
						$implodedArray = implode("','",$array);
						$implodedArray = "'$implodedArray'";
						$query = $query . "$ifEnd K.keyword IN ($implodedArray)";
					}
					echo $query;
					$query = $query .  " GROUP BY M.year ";
					
				$stmt = $db->query($query);
				if($stmt!=NULL)
					$worked = 1;
			}
		}
		catch(PDOException $e) 
		{
			echo 'ERROR: ' . $e->getMessage();
		}
	
	?>
<!DOCTYPE html>
<html>
	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="search">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Search / <small>Movies</small></h1>
				<p class="lead">
					If you've heard of it, we can find it. Try any combination of search parameters.
				</p>
				<hr>
			</div>

			<form id="search-form" action="search.php" method="GET" class="form-horizontal well">

				<h3>Basic Info</h3>
				<div id="basic-information">

					<!-- Title Field -->
					<div class="control-group">
						<label class="control-label" for="movie_title">Title:</label>
						<div class="controls">
							<input type="text" id="movie_title" name="movie_title" value="<?php echo $title; ?>" >
						</div>
					</div>

					<!-- Year Fields -->
					<div class="control-group">
						<label class="control-label" for="movie_year">Year:</label>
						<div class="controls">
							<input type="number" class="span1" id="movie_year_start" name="movie_year_start" value="<?php echo $start; ?>" >
							&nbsp;&nbsp;to&nbsp;&nbsp;
							<input type="number" class="span1" id="movie_year_end" name="movie_year_end" value="<?php echo $end; ?>" >
						</div>
					</div>

					<!-- MPAA Field -->
					<div class="control-group">
						<label class="control-label">MPAA:</label>
						<div class="controls">
							<label class="checkbox inline">
								<input type="checkbox" id="mpaa_rating_g" name="mpaa_rating[]" value="G"> G
							</label>
							<label class="checkbox inline">
								<input type="checkbox" id="mpaa_rating_pg" name="mpaa_rating[]" value="PG"> PG
							</label>
							<label class="checkbox inline">
								<input type="checkbox" id="mpaa_rating_pg13" name="mpaa_rating[]" value="PG-13"> PG-13
							</label>
							<label class="checkbox inline">
								<input type="checkbox" id="mpaa_rating_r" name="mpaa_rating[]" value="R"> R
							</label>
							<br />
							<label class="checkbox inline">
								<input type="checkbox" id="mpaa_rating_nc17" name="mpaa_rating[]" value="NC-17"> NC-17
							</label>
						</div>
					</div>
					

				</div>
				<h3>Rating information</h3>
				<div id="rating info">
					<!-- Rating  -->
					<div class="control-group">
						<label class="control-label">Min Average Rating:&nbsp;&nbsp;</label>
						<div class="controls">
							<input type="number" class="span1 id="rating" name="rating" value="<?php echo $atLeast;?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">With at least &nbsp;&nbsp;</label>
						<div class="controls">
							<input type="number" class="span2" id="count" name="count"  value="<?php echo $count; ?>" >
							&nbsp;&nbsp; Ratings
						</div>
					</div>
				</div>
				
				<h3>Staring</h3>
				<div id="stars">
					<div class="control-group">
						<label class="control-label">Actor/Actress:&nbsp;</label>
						<div class="controls">
							<input type="text" class="span2" id="Actorname" name="Actorname[]" placeholder="Kevin Bacon" />
							<input type="text" class="span2" id="Actorname" name="Actorname[]" placeholder="Kevin Bacon" />
							<input type="text" class="span2" id="Actorname" name="Actorname[]" placeholder="Kevin Bacon" />
							<input type="text" class="span2" id="Actorname" name="Actorname[]" placeholder="Kevin Bacon" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Directors:&nbsp;</label>
						<div class="controls">
							<input type="text" class="span2" id="Directorname" name="Directorname[]" placeholder="Steven" />
							<input type="text" class="span2" id="Directorname" name="Directorname[]" placeholder="Steven" />
							<input type="text" class="span2" id="Directorname" name="Directorname[]" placeholder="Steven" />
							<input type="text" class="span2" id="Directorname" name="Directorname[]" placeholder="Steven" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Producers:&nbsp;</label>
						<div class="controls">
							<input type="text" class="span2" id="Producersname" name="Producersname[]" placeholder="Alex Cadieux" />
							<input type="text" class="span2" id="Producersname" name="Producersname[]" placeholder="Alex Cadieux" />
							<input type="text" class="span2" id="Producersname" name="Producersname[]" placeholder="Alex Cadieux" />
							<input type="text" class="span2" id="Producersname" name="Producersname[]" placeholder="Alex Cadieux" />
						</div>
					</div>
				</div>
				<h3>Keywords</h3>
				<div id="keywords">
					<div class="control-group">
						<label class="control-label">Keyword:&nbsp;</label>
						<div class="controls">
							<input type="text" class="span2" id="keys" name="keys[]" placeholder="programming" />
							<input type="text" class="span2" id="keys" name="keys[]" placeholder="programming" />
							<input type="text" class="span2" id="keys" name="keys[]" placeholder="programming" />
							<input type="text" class="span2" id="keys" name="keys[]" placeholder="programming" />
						</div>
					</div>
				</div>
				<h3>Genres</h3>
				<div id="genres">

					<div class="controls-row">
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_action" name="genre[]" value="Action"> Action
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_adult" name="genre[]" value="Adult"> Adult
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_adventure" name="genre[]" value="Adventure"> Adventure
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_animation" name="genre[]" value="Animation"> Animation
						</label>
					</div>

					<div class="controls-row">
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_biography" name="genre[]" value="Biography"> Biography
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_comedy" name="genre[]" value="Comedy"> Comedy
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_crime" name="genre[]" value="Crime"> Crime
						</label>
						<label class="checkbox inline span1">
							<input type="checkbox" id="genre_documentary" name="genre[]" value="Documentary"> Documentary
						</label>
					</div>

				</div>

				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Search</button>
				</div>

			</form>
			<!-- Results-->
			<?php if ( $worked==1):?>
				<table id="query-results" class="table table-bordered table-striped table-hover">

					<!-- Column Names -->
					<thead>
						<tr>
						<?php
							$columnCount = $stmt->columnCount();
							for($a=0;$a<$columnCount;$a=$a+1)
							{
								echo '<th>';
								$meta = $stmt->getColumnMeta($a);
								print_r( $meta['name']);
								echo '</th>';
							}							
						?>
						<tr>
					</thead>

					<!-- Table Content -->
					<tbody>
					<?php
						$rowsCount=$stmt->rowCount();
						for($h=0;$h<$rowsCount;$h=$h+1)
						{
							echo '<tr>';
							$data = $stmt->fetch();
							for($i=0;$i<$columnCount;$i++)
							{
								if($i==0)
								{
									$prepedTitle = str_replace(" ", "+",$data[$i]);
									echo '<td><a href="movie.php?title='.$prepedTitle.'&year='.$data[1].'"</a>'.$data[0].'</td>';
								}
								else
								{
									echo'<td>';
									echo $data[$i];
									echo '</td>';
								}
							}
							echo '</tr>';
						}

					?>
					</tbody>

				</table>
			<?php endif; ?>
		</div>

	</body>

</html>