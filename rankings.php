<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Get database adapter instance
	require_once "inc/class_db.php";
	$db = new DB;

	// Evaluate 'Best Rated' query
	$query = "SELECT R.movie_title, R.movie_year, R.average_rating FROM RatingsCounts R WHERE R.count > 1000 ORDER BY R.average_rating DESC LIMIT 10;";
	$bestRated = $db->query($query);

	// Evaluate 'Most Rated' query
	//$query = "SELECT R.movie_title, R.movie_year, avg(age) FROM Ratings R INNER JOIN (SELECT A.movie_title, A.movie_year FROM RatingsCounts A WHERE A.count>5000) Z ON R.movie_title= Z.movie_title AND R.movie_year=Z.movie_year INNER JOIN Users U ON U.email = R.user_email GROUP BY movie_title, movie_year;";
	//$mostRated = $db->query($query);

	// Evaluate 'Special Selections' query
	//$query = "SELECT rat.movie_title,rat.movie_year, R.average_rating FROM Ratings rat INNER JOIN RatingsCounts R ON rat.movie_title=R.movie_title AND rat.movie_year=R.movie_year INNER JOIN TEMP_GoodTaste G On rat.user_email=G.user_email GROUP BY rat.movie_title,rat.movie_year ORDER BY R.average_rating DESC LIMIT 20;";
	//$specialSelections = $db->query($query);
?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="rankings">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Movies / <small>Rankings</small></h1>
				<p class="lead">
					What movies are the best of the best? Find out for yourself.
				</p>
				<hr>
			</div>

			<!-- Top 10 Overall -->
			<h3>Top 10 Overall</h3>
			<table id="query-results" class="table table-bordered table-striped table-hover">

				<!-- Table Head -->
				<thead>
					<tr>
						<th>Position</th>
						<th>Movie</th>
						<th>Year</th>
						<th>Avg. Rating</th>
					</tr>
				</thead>

				<!-- Table Content -->
				<tbody>
				<?php

					// Get the metadata about the results
					$rowsCount=$bestRated->rowCount();
					$columnCount=$bestRated->columnCount();

					// Iterate through each row of the results
					for($h=0;$h<$rowsCount;$h=$h+1)
					{
						echo "<tr>";

						// Print row cell
						$row = $h + 1;
						echo "<td><b>$row</b></td>";

						// Print fetched cells
						$data = $bestRated->fetch();
						for($i=0;$i<$columnCount;$i++)
						{
							echo '<td>';
							echo $data[$i];
							echo '</td>';
						}
						echo "</tr>";
					}

				?>
				</tbody>

			</table>

			<!-- Top 10 Action -->
			<h3>Top 10 in Action</h3>

		</div>

	</body>

	</html>