<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Messages
	$errorMsg;
	$successMsg;

	// User session object
	$user = $_SESSION["user_session"];

	// Check if new-list form was submitted
	if(isset($_POST["new-list-title"]))
	{
		// Make sure all fields were filled in
		if($_POST["new-list-title"] != "")
		{
			$title = $_POST["new-list-title"];

			// Attempt to create the list
			$result = $user->createNewList($title);

			// Check for success or error
			if($result)
			{
				$successMsg = "List '$title' was created successfully.";
			}
			else
			{
				$errorMsg = "There was an error creating list '$title'.";
			}
		}
		else
		{
			$errorMsg = "All fields must be filled in.";
		}
	}
?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<!-- Page-specific Javascript -->
	<script src="/js/lists.js"></script>

	<body id="lists">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Lists / <small>Manage</small></h1>
				<hr>
			</div>

			<!-- Error Message -->
			<?php if(isset($errorMsg)) :?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Error</h4>
					<?php echo $errorMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Success Message -->
			<?php if(isset($successMsg)) :?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Success</h4>
					<?php echo $successMsg; ?>
				</div>
			<?php endif; ?>

			<!-- New List Modal -->
			<div id="new-list-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="newListModalLabel" aria-hidden="true">

				<!-- Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="newListModalLabel">New List</h3>
				</div>

				<!-- Body -->
				<div class="modal-body">
					<form id="new-list-form" action="" method="POST" class="form-horizontal">
						<div class="control-group">
							<label class="control-label" for="new-list-title">Title:</label>
							<div class="controls">
								<input type="text" name="new-list-title" id="new-list-title" placeholder="Title" />
							</div>
						</div>
					</form>
				</div>

				<!-- Footer -->
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
					<input type="submit" id="submit-new-list" name="submit-new-list" class="btn btn-primary" value="Create List"/>
				</div>

			</div>

			<!-- Lists -->
			<?php 
				$stmt = $user->getAllLists();
				if($stmt != NULL):
			?>
				<table class="table table-striped">

					<thead>
						<tr>
							<th>Your Lists</th>
						</tr>
					</thead>

					<tbody>

						<?php
							$listResults = $stmt->fetchAll();

							// Iterate through all returned lists
							foreach($listResults as $row)
							{
								$rowID = $row["id"];
								$rowTitle = $row["title"];

								echo "<tr>";
									echo "<td><a href=\"list.php?id=$rowID\">$rowTitle</a></td>";
								echo "<tr>";
							}
						?>

					</tbody>

				</table>
			<?php endif; ?>

			<!-- Toolbar -->
			<div class="btn-group pull-right">
				<a href="#new-list-modal" role="button" class="btn" id="add-list" data-toggle="modal">New List</a>
			</div>

		</div>

	</body>

</html>