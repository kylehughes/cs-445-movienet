<?php
	
	/*
		Provides a facade for common database
		operations
	*/

	class DB
	{
		/*
			CONNECTION INFO
		*/

			private static $host = "codemonkeyupstairs.com";
			private static $dbname = "chm";
			private static $username = "cs445";
			private static $password = "password";

		/*
			FUNCTIONS
		*/

			// Returns the $stmt object of a query if successful
			// or NULL if unsuccessful
			public static function query($query)
			{
				try
				{
					// Attempt database connection
					$conn = new PDO("mysql:host=codemonkeyupstairs.com;dbname=chm", "cs445", "password");
					$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

					// Prepare and execute query
					$stmt = $conn->prepare($query);
					$stmt->execute();

					// Observe result to determine success

					if($stmt->rowCount() > 0)
					{
						return $stmt;
					}
					else
					{
						return NULL;
					}
				}
				catch(PDOException $err)
				{
					return NULL;
				}
			}
	}

?>