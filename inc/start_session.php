<?php

	// Require UserSession class
	require_once "inc/class_user_session.php";

	// Start or resume the session
	session_start();

	// Create & store UserSession if not set
	if(!isset($_SESSION["user_session"]))
	{
		$_SESSION["user_session"] = new UserSession;
	}

?>