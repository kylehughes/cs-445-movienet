<?php
  
  // Get user session
  if(!isset($user))
  {
    $user = $_SESSION["user_session"];
  }

?>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">

      <!-- Title -->
      <a class="brand" href="/">MovieNet</a>

      <!-- Main Navigation -->
      <ul class="nav">
        <li><a href="/search.php">Search</a></li>
        <li><a href="/rankings.php">Rankings</a></li>
        <li class="dropdown">

          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Social
            <b class="caret"></b>
          </a>

          <ul class="dropdown-menu">
            <li><a tabindex="-1" href="/friends.php">Friends</a></li>
            <li><a tabindex="-1" href="/lists.php">Lists</a></li>
          </ul>

        </li>
      </ul>

      <!-- Secondary Navigation -->
      <ul class="nav pull-right">

        <?php if(!$user->isAuthenticated) :?>
          <!-- Unauthenticated Menu -->
          <li><a href="/register.php">Register</a></li>
          <li><a href="/login.php">Login</a></li>
        <?php else :?>
          <!-- Authenticated Menu -->
          <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              My Account
              <b class="caret"></b>
            </a>

            <ul class="dropdown-menu">
              <li><a tabindex="-1" href="/profile.php">Profile</a></li>
              <li><a tabindex="-1" href="/admin">Admin</a></li>
              <li class="divider"></li>
              <li><a tabindex="-1" href="/logout.php">Logout</a></li>
            </ul>

          </li>
        <?php endif; ?>

      </ul>

    </div>
  </div>
</div>