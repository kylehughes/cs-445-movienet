<head>
    <title>MovieNet</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <!-- Bootstrap -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- Local -->
    <link href="/css/screen.css" rel="stylesheet" media="screen">
</head>