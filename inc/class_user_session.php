<?php

	require_once "class_db.php";

	/*
		Represents a session for a user (authenticated
		or not)
	*/

	class UserSession 
	{
		/*
			PROPERTIES
		*/
			
			private $db; // Database Adapter

			public $email;
			public $username;

			public $isAuthenticated = false;

		/*
			METHODS
		*/

			// Constructor
			public function __construct()
			{
				$this->db = new DB;
			}

			// Returns 1 if successful (and sets authentication), returns -1 if failed because conflict,
			// 0 if it doesn't work for any other reason.
			public function register($email, $username, $password, $age, $sex, $location)
			{
				// Compile query to check for existence of email address
				$query = "SELECT * FROM Users U WHERE U.`email` = '$email';";

				// Execute query
				$stmt = $this->db->query($query);

				// Check if email already exists
				if($stmt == NULL)
				{
					// Compile user creation query
					$query = "INSERT INTO Users VALUES ('$email', '$username', '$password', '$age', '$sex', '$location', 0);";

					// Execute query
					$stmt = $this->db->query($query);

					// Check if registration went through
					if($stmt != NULL)
					{
						// Assign user properties
						$this->email = $email;
						$this->username = $username;

						// Set authentication status
						$this->isAuthenticated = true;

						return 1;
					}
					else
					{
						return 0; 
					}
				}
				else
				{
					return -1;
				}
			}

			// Returns 'true' if successful, false if not
			public function authenticate($email, $password)
			{
				// Compile authentication query
				$query = "SELECT * FROM Users U WHERE U.`email` = '$email' AND U.`password` = '$password';";

				// Execute query
				$stmt = $this->db->query($query);

				// Authenticate the user or not
				if($stmt != NULL)
				{
					// Get singular result as an assoc. array
					$result = $stmt->fetch(PDO::FETCH_ASSOC);

					// Assign properties from DB
					$this->email = $result["email"];
					$this->username = $result["username"];

					// Set authentication status
					$this->isAuthenticated = true;
				}
				else
				{
					// Set authentication status
					$this->isAuthenticated = false;
				}

				return $this->isAuthenticated;
			}

			// Resets all variables
			public function unauthenticate()
			{
				unset($email);
				unset($username);
				$this->isAuthenticated = false;
			}

			// Returns true if successful, false if not
			public function rateMovie($title, $year, $rating)
			{
				$email = $this->email;

				// Compile rating insertion query
				$query = "INSERT INTO Ratings VALUES (NOW(), '$rating', '$email', '$title', '$year');";

				// Execute query
				$stmt = $this->db->query($query);

				if($stmt != NULL)
				{
					// Compile rating-summary getting query
					$query = "SELECT *
							  FROM RatingsCounts R
							  WHERE R.`movie_title` = '$title' AND
							  		R.`movie_year` = '$year';";

					// Execute query
					$stmt = $this->db->query($query);

					if($stmt != NULL)
					{
						$result = $stmt->fetch(PDO::FETCH_ASSOC);

						$totalCount = $result["count"];
						$averageRating = $result["average_rating"];

						// Compute new count and average
						$averageRating = (($averageRating * intval($totalCount)) + intval($rating))/(intval($totalCount) + 1);
						$totalCount = intval($totalCount) + 1;

						// Compile rating-summary updating query
						$query = "UPDATE RatingsCounts
								  SET count = $totalCount,
								  	  average_rating = $averageRating
								  WHERE movie_title = '$title' AND movie_year = '$year'";

						// Execute query
						$stmt = $this->db->query($query);

						return ($stmt != NULL);
					}
					else
					{
						
						// Compile rating-summary insertion query
						$query = "INSERT INTO RatingsCounts
								  VALUES ('$title', $year, 1, $rating);";

						// Execute query
						$stmt = $this->db->query($query);

						return ($stmt != NULL);
					}
				}
				else
				{
					return false;
				}
			}

			// Returns statement object if true, NULL if not
			public function hasRatedMovie($title, $year)
			{
				$email = $this->email;

				// Compile rating checking query
				$query = "SELECT * 
						  FROM Ratings R 
						  WHERE R.`user_email` = '$email' AND
						  		R.`movie_title` = '$title' AND
						  		R.`movie_year` = '$year';";

				// Execute query
				$stmt = $this->db->query($query);

				return $stmt;
			}

			// Returns true if successful, false if not
			public function createNewList($title)
			{
				$email = $this->email;

				// Compile list creation query
				$query = "INSERT INTO Lists
						  VALUES (NULL, '$title', '$email');";

				// Execute query
				$stmt = $this->db->query($query);

				return ($stmt != NULL);
			}

			// Returns the statement object
			public function getAllLists()
			{
				$email = $this->email;

				// Compile list-fetching query
				$query = "SELECT *
						  FROM Lists L
						  WHERE L.`user_email` = '$email';";

				// Execute query
				$stmt = $this->db->query($query);

				return $stmt;
			}

			// Returns true if successful, false if not
			public function addMovieToList($listID, $title, $year)
			{
				// Compile movie-insertion query
				$query = "INSERT INTO Catalog_Lists
						  VALUES (NULL, 0, '$title', '$year', '$listID');";

				// Execute query
				$stmt = $this->db->query($query);

				return ($stmt != NULL);
			}

			// Returns true if successful, false if not
			public function removeMovieFromList($listID, $title, $year)
			{
				// Compile movie-deletion query
				$query = "DELETE FROM Catalog_Lists
						  WHERE `list_id` = $listID AND
						  		`movie_title` = '$title' AND
						  		`movie_year` = '$year';";

				// Execute query
				$stmt = $this->db->query($query);

				return ($stmt != NULL);
			}
	}

?>