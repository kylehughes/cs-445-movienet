<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	require_once"inc/class_db.php";

	// DB adapter
	$db = new DB;

	$user = $_SESSION["user_session"];

	// Pre-determined page fields
	$listID;
	$listTitle;
	$listUserEmail;
	$movieResults;

	// Check for list ID
	if(isset($_GET["id"]))
	{
		$listID = $_GET["id"];

		// Check if deletion form was submitted
		if(isset($_POST["deletion-title"]))
		{
			// Attempt to remove movie from the list
			$result = $user->removeMovieFromList($listID, $_POST["deletion-title"], $_POST["deletion-year"]);

			// Tell user of result
			if($result)
			{
				$successMsg = "Movie was successfully deleted.";
			}
			else
			{
				$errorMsg = "Movie was not deleted, please try again.";
			}
		}

		// Compile list-information query
		$query = "SELECT *
				  FROM Lists L
				  WHERE L.`id` = $listID;";

		// Execute query
		$stmt = $db->query($query);
		$listResult = $stmt->fetch(PDO::FETCH_ASSOC);

		// Get list properties
		$listTitle = $listResult["title"];
		$listUserEmail = $listResult["user_email"];

		// Compile movie-getting query
		$query = "SELECT *
				  FROM Catalog_Lists C
				  WHERE C.`list_id` = $listID;";

		// Execute query
		$stmt = $db->query($query);
		if($stmt != NULL)
		{
			$movieResults = $stmt->fetchAll();
		}
	}

?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<!-- Page-specific Javascript -->
	<script src="/js/list.js"></script>

	<body id="list">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1><?php echo $listTitle; ?> / <small><?php echo $listUserEmail; ?></small></h1>
				<hr>
			</div>

			<!-- Error Message -->
			<?php if(isset($errorMsg)) :?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Error</h4>
					<?php echo $errorMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Success Message -->
			<?php if(isset($successMsg)) :?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Success</h4>
					<?php echo $successMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Movies -->
			<?php if(isset($movieResults)): ?>

				<table class="table table-striped">

					<thead>
						<tr>
							<th>Movie</th>
							<th style="text-align:right;">Delete?</th>
						</tr>
					</thead>

					<tbody>

						<?php
							// Iterate through all returned movies
							foreach($movieResults as $row)
							{
								$rowTitle = $row["movie_title"];
								$rowYear = $row["movie_year"];

								echo "<tr data-title=\"$rowTitle\" data-year=\"$rowYear\">";
									echo "<td><a href=\"movie.php?title=$rowTitle&year=$rowYear\">$rowTitle</a>&nbsp;&nbsp;<small><em>($rowYear)</em></small></td>";
									echo "<td><button class=\"close delete-button\">&times;</button></td>";
								echo "<tr>";
							}
						?>

					</tbody>

				</table>

			<?php endif; ?>

			<!-- Deletion Form -->
			<form id="deletion-form" style="visiblity:hidden" action="" method="POST">
				<input type="hidden" name="deletion-title" id="deletion-title" value=""/>
				<input type="hidden" name="deletion-year" id="deletion-year" value=""/>
			</form>

		</div>

	</body>

</html>