<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Error Message variable
	$errorMsg;

	$successPage = "profile.php"; // Success page location
	$user = $_SESSION["user_session"]; // User session object

	// Ensure user is not already logged in
	if(!$user->isAuthenticated)
	{
		// Check if form submitted
		if(isset($_POST["submit"]))
		{
			// Check if all forms filled in
			if($_POST["email"] != "" && $_POST["password"] != "")
			{
				// Attempt to autheticate
				if($user->authenticate($_POST["email"], $_POST["password"]))
				{
					header("Location: $successPage");
				}
				else
				{
					// Login failed, display error
					$errorMsg = "Wrong email and password combination.";
				}
			}
			else
			{
				// Not all fields (or foul play), display error
				$errorMsg = "You must fill in all fields.";
			}
		}
	}
	else
	{
		// Redirect (because logged in)
		header("Location: $successPage");
	}
?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="login">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Login</small></h1>
				<hr>
			</div>

			<!-- Error Message -->
			<?php if(isset($errorMsg)) :?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Error</h4>
					<?php echo $errorMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Login Form -->
			<form class="form-horizontal" method="POST" action="">

				<!-- Email -->
				<div class="control-group">
					<label class="control-label" for="email">Email:</label>
					<div class="controls">
						<input type="email" name="email" id="email" pattern="[^ @]*@[^ @]*" placeholder="Email" />
					</div>
				</div>

				<!-- Password -->
				<div class="control-group">
					<label class="control-label" for="password">Password:</label>
					<div class="controls">
						<input type="password" name="password" id="password" placeholder="Password" />
					</div>
				</div>

				<!-- Form Actions -->
				<div class="form-actions">
					<input type="submit" id="submit" name="submit" class="btn btn-primary" value="Login"/>
					&nbsp;&nbsp;or&nbsp;&nbsp;
					<a class="btn">Register</a>
				</div>

			</form>

		</div>

	</body>

</html>