<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="profile">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>krhughes / <small>Profile</small></h1>
				<hr>
			</div>

			<!-- Overview -->
			<div id="overview">

				<h3>Overview</h3>

				<table>

					<!-- Email -->
					<tr>
						<td class="field">Email:</td>
						<td class="datum">kyle@codemonkeyupstairs.com</td>
					</tr>

					<!-- Age -->
					<tr>
						<td class="field">Age:</td>
						<td class="datum">20</td>
					</tr>

					<!-- Sex -->
					<tr>
						<td class="field">Sex:</td>
						<td class="datum">Male</td>
					</tr>

					<!-- Location (lol get it) -->
					<tr>
						<td class="field">Location:</td>
						<td class="datum">Massachusetts</td>
					</tr>

				</table>

			</div>

			<div id="activity">

				<h3>Activity</h3>

				<!-- Tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#ratings" data-toggle="tab">Ratings</a></li>
					<li><a href="#reviews" data-toggle="tab">Reviews</a></li>
				</ul>

				<!-- Tab Content -->
				<div class="tab-content">

					<!-- Ratings Pane -->
					<div class="tab-pane active" id="ratings">

						<table class="table table-striped">

							<thead>
								<tr>
									<th>Movie</th>
									<th>Rating</th>
								</tr>
							</thead>

							<tbody>

								<tr>
									<td>Mission Impossible: Ghost Protocol <small><em>&nbsp;(2012)</em></small></td>
									<td class="rating">8 <small>/ 10</small></td>
								</tr>

								<tr>
									<td>Batman Begins Again Again <small><em>&nbsp;(2018)</em></small></td>
									<td class="rating">1 <small>/ 10</small></td>
								</tr>

							</tbody>

						</table>

					</div>

					<!-- Reviews Pane -->
					<div class="tab-pane" id="reviews">



					</div>

				</div>

			</div>

		</div>

	</body>

</html>