$(function()
{
	// Set action for "Create List" button
	$('.delete-button').each(function()
	{
		$(this).click(function()
		{
			var parentRow = $(this).parent().parent();
			var title = parentRow.attr('data-title');
			var year = parentRow.attr('data-year');

			$('#deletion-title').val(title);
			$('#deletion-year').val(year);

			$('#deletion-form').submit();
		});
	});
});