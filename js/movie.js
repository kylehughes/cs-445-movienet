$(function()
{

	// Setup Tooltip for MPAA Rating
	$('#mpaa-rating').tooltip();

	// Get Movie Poster from Google
	var googleAPI = "https://ajax.googleapis.com/ajax/services/search/images?";
	var searchString = movieTitle + " " + movieYear + " movie poster";
	$.getJSON( 
		googleAPI, 
		{
			v: "1.0",
			q: searchString
		}
	)
	.done(function(data) 
	{
		var posterURL = data.responseData.results[0].url;
		$('#poster').attr('src', posterURL);
	});

});