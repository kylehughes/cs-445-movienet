<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Error Message variable
	$errorMsg;

	$successPage = "profile.php"; // Success page location
	$user = $_SESSION["user_session"]; // User session object

	// Ensure user is not already logged in
	if(!$user->isAuthenticated)
	{
		// Check if form submitted
		if(isset($_POST["submit"]))
		{
			// Check if all forms are filled in
			if($_POST["email"] != "" && $_POST["username"] != "" && $_POST["password"] != "" && $_POST["age"] != "" && $_POST["sex"] != "" && $_POST["location"] != "")
			{
				// Attempt to register
				$result = $user->register($_POST["email"], $_POST["username"], $_POST["password"], $_POST["age"], $_POST["sex"], $_POST["location"]);

				// Provide response based on registration result
				if($result == 1)
				{
					header("Location: $successPage");
				}
				else if($result == 0)
				{
					$errorMsg = "Registration failed, please try again.";
				}
				else if($result == -1)
				{
					$errorMsg = "That email address is taken.";
				}
			}
			else
			{
				$errorMsg = "All fields must be filled in.";
			}
		}
	}
	else
	{
		header("Location: $successPage");
	}
?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="register">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Register</small></h1>
				<hr>
			</div>

			<!-- Error Message -->
			<?php if(isset($errorMsg)) :?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Error</h4>
					<?php echo $errorMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Login Form -->
			<form class="form-horizontal" method="POST" action="">

				<!-- Email -->
				<div class="control-group">
					<label class="control-label" for="email">Email:</label>
					<div class="controls">
						<input type="email" id="email" name="email" pattern="[^ @]*@[^ @]*" placeholder="Email" />
					</div>
				</div>

				<!-- Username -->
				<div class="control-group">
					<label class="control-label" for="username">Username:</label>
					<div class="controls">
						<input type="text" id="username" name="username" placeholder="Username" />
					</div>
				</div>

				<!-- Password -->
				<div class="control-group">
					<label class="control-label" for="password">Password:</label>
					<div class="controls">
						<input type="password" name="password" id="password" placeholder="Password" />
					</div>
				</div>

				<!-- Age -->
				<div class="control-group">
					<label class="control-label" for="age">Age:</label>
					<div class="controls">
						<input class="input-small" type="number" name="age" id="age" placeholder="Age" />
					</div>
				</div>

				<!-- Sex -->
				<div class="control-group">
					<label class="control-label" for="sex">Sex:</label>
					<div class="controls">
						<select id="sex" name="sex" class="input-small">
							<option>Male</option>
							<option>Female</option>
						</select>
					</div>
				</div>

				<!-- Location -->
				<div class="control-group">
					<label class="control-label" for="location">Location:</label>
					<div class="controls">
						<select id="location" name="location">
							<option value="Alabama">Alabama</option>
							<option value="Alaska">Alaska</option>
							<option value="Arizona">Arizona</option>
							<option value="Arkansas">Arkansas</option>
							<option value="California">California</option>
							<option value="Colorado">Colorado</option>
							<option value="Connecticut">Connecticut</option>
							<option value="Delaware">Delaware</option>
							<option value="Florida">Florida</option>
							<option value="Georgia">Georgia</option>
							<option value="Hawaii">Hawaii</option>
							<option value="Idaho">Idaho</option>
							<option value="Illinois">Illinois</option>
							<option value="Indiana">Indiana</option>
							<option value="Iowa">Iowa</option>
							<option value="Kansas">Kansas</option>
							<option value="Kentucky">Kentucky</option>
							<option value="Louisiana">Louisiana</option>
							<option value="Maine">Maine</option>
							<option value="Maryland">Maryland</option>
							<option value="Massachusetts">Massachusetts</option>
							<option value="Michigan">Michigan</option>
							<option value="Minnesota">Minnesota</option>
							<option value="Mississippi">Mississippi</option>
							<option value="Missouri">Missouri</option>
							<option value="Montana">Montana</option>
							<option value="Nebraska">Nebraska</option>
							<option value="Nevada">Nevada</option>
							<option value="New Hampshire">New Hampshire</option>
							<option value="New Jersey">New Jersey</option>
							<option value="New Mexico">New Mexico</option>
							<option value="New York">New York</option>
							<option value="North Carolina">North Carolina</option>
							<option value="North Dakota">North Dakota</option>
							<option value="Ohio">Ohio</option>
							<option value="Oklahoma">Oklahoma</option>
							<option value="Oregon">Oregon</option>
							<option value="Pennsylvania">Pennsylvania</option>
							<option value="Rhode Island">Rhode Island</option>
							<option value="South Carolina">South Carolina</option>
							<option value="South Dakota">South Dakota</option>
							<option value="Tennessee">Tennessee</option>
							<option value="Texas">Texas</option>
							<option value="Utah">Utah</option>
							<option value="Vermont">Vermont</option>
							<option value="Virginia">Virginia</option>
							<option value="Washington">Washington</option>
							<option value="West Virginia">West Virginia</option>
							<option value="Wisconsin">Wisconsin</option>
							<option value="Wyoming">Wyoming</option>
						</select>
					</div>
				</div>

				<!-- Form Actions -->
				<div class="form-actions">
					<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Register"/>
					&nbsp;&nbsp;or&nbsp;&nbsp;
					<a href="/login.php" class="btn">Login</a>
				</div>

			</form>

		</div>

	</body>

</html>