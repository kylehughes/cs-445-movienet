<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Get user session
	$user = $_SESSION["user_session"];

	// Check if user is authenticated
	if($user->isAuthenticated)
	{
		// Unauthenticate user
		$user->unauthenticate();

		// Redirect
		header("Location: index.php");
	}
	else
	{
		// Redirect
		header("Location: index.php");
	}
?>