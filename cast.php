<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body id="cast">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Tom Cruise / <small>Appearances</small></h1>
				<hr>
			</div>

			<!-- Appearances -->
			<div>

				<table class="table table-striped">

					<thead>
						<tr>
							<th>Year</th>
							<th>Movie</th>
							<th>Role</th>
							<th>Job</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td>2012</td>
							<td>Mission Impossible: Ghost Protocol</td>
							<td>Fezzik</td>
							<td>Actor</td>
						</tr>

						<tr>
							<td>2010</td>
							<td>The Life of Tom Cruise</td>
							<td>N/A</td>
							<td>Director</td>
						</tr>

					</tbody>

				</table>

			</div>

		</div>

	</body>

</html>