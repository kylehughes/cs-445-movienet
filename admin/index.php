<?php

	// MANUALLY DOING NORMAL SESSION STUFF PLEASE IGNORE
	// Require UserSession class
	require_once "../inc/class_user_session.php";

	// Start or resume the session
	session_start();

	// Create & store UserSession if not set
	if(!isset($_SESSION["user_session"]))
	{
		$_SESSION["user_session"] = new UserSession;
	}
	// END OF NORMAL SESSION STUFF

	try 
	{
		$conn = new PDO('mysql:host=codemonkeyupstairs.com;dbname=chm', 'cs445', 'password');
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$before = time();
		$worked=0;
		$stmt ='';
		if(count($_GET) > 0)
		{
			$stmt = $conn->prepare($_GET["query-box"]);
			$stmt->execute();
			$worked=1;
		}
		$after = time();
	} 
	catch(PDOException $e) 
	{
	}
?>
<!DOCTYPE html>
<html>




	<!-- <head> tag -->
	<?php include_once '../inc/doc_head.php'; ?>

	<body id="admin">

		<!-- Navigation Bar -->
		<?php include_once '../inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>Admin / <small>Queries</small></h1>
				<p class="lead">
					The vast expanses of our database: all just a SQL query away.
				</p>
				<hr>
			</div>

			<div class="well">

				<!-- Query Form -->
				<div id="query-form" class="input-append">
					<form action="index.php" method="GET">
						<input id="query-box" name="query-box" type="text" />
						<button type="submit" class="btn btn-primary btn-large" type="submit">Execute</button>
					</form>
				</div>

				<?php if ($worked==1):?>
					<!-- Query Statistics -->
					<div id="query-statistics">
						<h3>Statistics</h3>
						<ul>
							<li><b>Execution Time:&nbsp;&nbsp;&nbsp;</b><code><?php echo $after - $before; echo' seconds'; ?></code>
							<li><b>Rows Returned:&nbsp;&nbsp;&nbsp;</b><code><?php echo $stmt->rowCount();  ?></code>
						</ul>
					</div>
				<?php endif; ?>

			</div>

			<!-- Query Results -->
			<?php if ( $worked==1):?>
				<table id="query-results" class="table table-bordered table-striped table-hover">

					<!-- Column Names -->
					<thead>
						<tr>
						<?php
							$columnCount=$stmt->columnCount();
							for($a=0;$a<$columnCount;$a=$a+1)
							{
								echo '<th>';
								$meta = $stmt->getColumnMeta($a);
								print_r( $meta['name']);
								echo '</th>';
							}							
						?>
						<tr>
					</thead>

					<!-- Table Content -->
					<tbody>
					<?php
						$rowsCount=$stmt->rowCount();
						for($h=0;$h<$rowsCount;$h=$h+1)
						{
							echo '<tr>';
							$data = $stmt->fetch();
							for($i=0;$i<$columnCount;$i++)
							{
								echo '<td>';
								echo $data[$i];
								echo '</td>';
							}
							echo '</tr>';
						}

					?>
					</tbody>

				</table>
			<?php endif; ?>

		</div>

	</body>

</html>