<?php

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<body>

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Hero Unit -->
			<div class="hero-unit">
				<h1>Welcome to MovieNet</h1>
				<p>
					The best place to find information on movies that you can probably find elsewhere,
					and connect with friends you probably don't have. This will eventually be replaced with somethin
					cooler.
				</p>
				<p>
					<a class="btn btn-primary btn-large">Register</a>
					&nbsp;&nbsp;or&nbsp;&nbsp;
					<a class="btn btn-large">Sign In</a>
				</p>
			</div>

			<!-- Feature List -->
			<ul id="feature-list" class="thumbnails">

				<li class="span4">
					<div class="thumbnail">
						<img src="http://placehold.it/300x200" alt="">
						<div class="caption">
							<h3>Information Heaven</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae malesuada risus.
								Cras quis nisi vitae augue scelerisque mattis. Vivamus sapien est, pellentesque at blandit at,
								egestas commodo tellus. Morbi purus leo, varius nec tincidunt in, iaculis id augue.
							</p>
						</div>
					</div>
				</li>

				<li class="span4">
					<div class="thumbnail">
						<img src="http://placehold.it/300x200" alt="">
						<div class="caption">
							<h3>Litany of Lists</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae malesuada risus.
								Cras quis nisi vitae augue scelerisque mattis. Vivamus sapien est, pellentesque at blandit at,
								egestas commodo tellus. Morbi purus leo, varius nec tincidunt in, iaculis id augue.
							</p>
						</div>
					</div>
				</li>

				<li class="span4">
					<div class="thumbnail">
						<img src="http://placehold.it/300x200" alt="">
						<div class="caption">
							<h3>Fantastic Friends</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae malesuada risus.
								Cras quis nisi vitae augue scelerisque mattis. Vivamus sapien est, pellentesque at blandit at,
								egestas commodo tellus. Morbi purus leo, varius nec tincidunt in, iaculis id augue.
							</p>
						</div>
					</div>
				</li>

			</ul>

		</div>

	</body>

</html>