<?php
	/*
		GET PARAMETERS:
		$_GET["title"] = title of the movie
		$_GET["year"] = year of the movie

		Example URL: /movie.php?title=Batman&year=1998
	*/ 

	// Start / Resume session (also includes UserSession)
	require_once "inc/start_session.php";

	// Messages
	$errorMsg;
	$successMsg;

	// Page checks
	$checkHasRated = false;

	// Page properties
	$userMovieRating;
	$userLists;

 	// User session object
	$user = $_SESSION["user_session"];

	// Check if rating form was submitted
	if(isset($_POST["submit-rating"]))
	{
		// Attempt to submit rating
		$result = $user->rateMovie($_GET["title"], $_GET["year"], $_POST["user-rating"]);

		// Check for success or failure
		if($result)
		{
			$successMsg = "Your rating was submitted.";
		}
		else
		{
			$errorMsg = "Your rating failed to submit. Please try again.";
		}
	}

	// Check if list form was submitted
	if(isset($_POST["submit-list"]))
	{
		$title = $_GET["title"];

		// Attempt to save movie to list
		$result = $user->addMovieToList($_POST["user-list-id"], $title, $_GET["year"]);

		// Check for success or failure
		if($result)
		{
			$successMsg = "$title was added to the list.";
		}
		else
		{
			$errorMsg = "$title is already in that list, silly!";
		}
	}

	// Login'd-only operations
	if($user->isAuthenticated)
	{
		// Check if user has rated the movie before
		$stmt = $user->hasRatedMovie($_GET["title"], $_GET["year"]);
		if($stmt)
		{
			$checkHasRated = true;
			$userMovieRatingResult = $stmt->fetch(PDO::FETCH_ASSOC);
			$userMovieRating = $userMovieRatingResult["rating"];
		}

		// Get all lists of the user
		$stmt = $user->getAllLists();
		if($stmt)
		{
			$userLists = $stmt->fetchAll();
		}
	}
?>
<!DOCTYPE html>
<html>

	<!-- <head> tag -->
	<?php include_once 'inc/doc_head.php'; ?>

	<!-- Page-specific Javascript -->
	<script type="text/javascript">
		var movieTitle = <?php echo "'".$_GET["title"]."'"; ?>;
		var movieYear = <?php echo "'".$_GET["year"]."'"; ?>;
	</script>
	<script src="/js/movie.js"></script>

	<body id="movie-single">

		<!-- Navigation Bar -->
		<?php include_once 'inc/doc_navbar.php'; ?>

		<!-- Content Container -->
		<div id="content" class="container">

			<!-- Page Title -->
			<div class="page-title">
				<h1>The Princess Bride / <small>1984</small></h1>
				<hr>
			</div>

			<!-- Error Message -->
			<?php if(isset($errorMsg)) :?>
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Error</h4>
					<?php echo $errorMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Success Message -->
			<?php if(isset($successMsg)) :?>
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4>Success</h4>
					<?php echo $successMsg; ?>
				</div>
			<?php endif; ?>

			<!-- Movie -->
			<div id="movie" class="row">

				<!-- Movie Poster / Controls -->
				<div class="span4">

					<!-- Poster -->
					<img id="poster" class="img-polaroid" src="http://placehold.it/270x400"/>

					<!-- Controls -->
					<?php if($user->isAuthenticated) :?>
						<div id="controls" class="well well-small">

							<!-- Rate Form -->
							<form id="rating-control" class="form-horizontal" method="POST" action="">
								<div class="control-group">
									<label class="control-label" for="user-rating">Rate:</label>
									<div class="controls">
										<select id="user-rating" name="user-rating" class="input-small" <?php if($checkHasRated) { echo "disabled"; } ?> >
											<?php for($i=1; $i<=10; $i++): ?>
												<option value="<?php echo $i; ?>" <?php if($checkHasRated && $i==$userMovieRating) { echo "selected"; } ?>><?php echo $i; ?></option>
											<?php endfor; ?>
										</select>
										<input type="submit" class="btn pull-right" id="submit-rating" name="submit-rating" value="Save Rating" <?php if($checkHasRated) { echo "disabled"; } ?> />
									</div>
								</div>
							</form>

							<hr>

							<!-- List Form -->
							<form id="list-control" class="form-horizontal" method="POST" action="">
								<div class="control-group">
									<label class="control-label" for="user-list-id">List:</label>
									<div class="controls">
										<select id="user-list-id" name="user-list-id" class="input-medium">
											<?php 
												foreach($userLists as $row):
													$listID = $row["id"];
													$listTitle = $row["title"];
											?>
												<option value="<?php echo $listID; ?>"><?php echo $listTitle; ?></option>
											<?php endforeach; ?>
										</select>
										<input type="submit" class="btn pull-right" id="submit-list" name="submit-list" value="Add" />
									</div>
								</div>
							</form>

						</div>
					<?php endif; ?>

				</div>

				<!-- Movie Overview -->
				<div class="span8">

					<h3>Overview</h3>

					<!-- Details -->
					<table id="details" cellspacing="100px">

						<!-- MPAA Rating -->
						<tr>
							<td class="field">MPAA:</td>
							<td class="datum">
								<a id="mpaa-rating" href="#" data-toggle="tooltip" data-placement="right" title data-original-title="Restricted. Children Under 17 Require Accompanying Parent or Adult Guardian.">R</a>
							</td>
						</tr>

						<!-- Runtime -->
						<tr>
							<td class="field">Runtime:</td>
							<td class="datum">120 minutes</td>
						</tr>

						<!-- Genres -->
						<tr>
							<td class="field">Genre(s):</td>
							<td class="datum">Action, Adventure, Comedy, Romance, Horror, Kids, Drama</td>
						</tr>

						<!-- Keywords -->
						<tr>
							<td class="field">Keyword(s):</td>
							<td class="datum">Fun, Cool, More, Keywords</td>
						</tr>

						<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

						<!-- Producer -->
						<tr>
							<td class="field">Producer:</td>
							<td class="datum">John Doe</td>
						</tr>

						<!-- Director -->
						<tr>
							<td class="field">Director:</td>
							<td class="datum">John Doe II</td>
						</tr>

						<!-- Editor -->
						<tr>
							<td class="field">Editor:</td>
							<td class="datum">John Doe III</td>
						</tr>

						<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

						<!-- Average Rating -->
						<tr>
							<td class="field">Avg. Rating:</td>
							<td class="datum">8.63</td>
						</tr>

					</table>

				</div>

			</div>

			<!-- Cast (everyone not a Producer, Director, or Editor) -->
			<div id="cast">

				<h3>Cast</h3>

				<table class="table table-striped">

					<thead>
						<tr>
							<th>Name</th>
							<th>Role</th>
							<th>Job</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td>Andre the Giant</td>
							<td>Fezzik</td>
							<td>Actor</td>
						</tr>

						<tr>
							<td>Robin Wright</td>
							<td>The Princess Bride</td>
							<td>Actor</td>
						</tr>

					</tbody>

				</table>

			</div>

		</div>

	</body>

</html>